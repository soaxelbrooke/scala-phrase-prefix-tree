package com.axelbrooke.phrase

case class PhraseTreeTestCase(tokens: Seq[String], rawTree: String, phrases: Set[String])

object PhraseDetectTestCases {

  val cases: Seq[PhraseTreeTestCase] = Seq(
    PhraseTreeTestCase(
      Seq("this", "is", "my", "test", "string", ",", "it", "is", "about", "a", "root", "child"),
      "root\n\tchild",
      Set("root child")
    ),
    PhraseTreeTestCase(
      "Something I worry about is that friends, go something I dunno ducks".toLowerCase.split(" "),
      "root\n\tchild",
      Set()
    ),
    PhraseTreeTestCase(
      "Something I worry about is that friends, go something I dunno ducks".toLowerCase.split(" "),
      "i\n\tworry\n\t\tabout\n\tdunno",
      Set("i dunno", "i worry about", "i worry")
    )
  )

}
